# Hackaton Ionic-3 Fiducia-GAD

## Vorbereitung:
Vorrausetzung: Mac Betriebsystem (OSX, MacOS 10+)
Installation von folgender Software
- Nodejs mit NPM (am besten per `Homebrew`)
- VSCode/WebStorm oder anderer Editor
- Chrome Browser
- Android SDK
- XCode

## Installation Ionic:
```
  $ npm install -g cordova ionic
```

```
  $ ionic start
```

```
  $ cd {app name}
```

```
  $ ionic serve
```

## Folder Structure

```sh
hooks # cordova hooks
  └── README.md
node_modules
  └── ...
platforms # cordova platforms
  └── ios
      └── # the ios project
  └── android
      └── # the android project
  └── platforms.json
plugins
  └── # cordova plugins
resources
  └── # platform specific Icons/Splashscreens
src
  └── app
    └── app.component.ts
    └── app.html
    └── app.module.ts
    └── app.scss
    └── maint.ts
  ├── assets # webapp assets
  └── pages # page components
    └── home
      └── home.html
      └── home.scss
      └── home.ts
  ├── theme
  ├── index.html
  ├── manifest.json # for PWA
  └── service-worker.js # for PWA
www
  └── # temp folder for cordova
```

## Basics Angular
- explain files in app/
- architecture overview: https://angular.io/docs/ts/latest/guide/architecture.html
- display data
  - simple string interpolation
    - data binding
    - explain syntax `{{}} []`
  - ngFor with array (explain *)
  - ngIf
- user input
  - button, explain `()` syntax
  - use `#bank`, skip forms
  - explain events
  - toggle button (ngIf)

## Basics Ionic
- styling
  - custom: `css` (skip for now)
- theming
  - customize `variables.scss`
  - add `color=""`
- navigation (explain with list page)

## Basiscs Cordova
- explain platforms
- explain plugins (show installed plugins)
- explain `config.xml`


# Prep **real** App
