import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  name: String = 'Peter';
  banks: Array<String> = ['Münchner Bank', 'Volksbank Stuttgart', 'Donau-Iller Bank'];
  isAvailable: boolean = false;

  constructor(public navCtrl: NavController) {}

  addBank (bank, event) {
    console.log('hello world!', event)
    this.banks.push(bank);
  }

}
